package com.example.alejandro.myapplication.poifiltering;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alejandro.myapplication.R;

import java.util.ArrayList;
import es.situm.sdk.model.cartography.Poi;

/**
 * Created by alejandro on 8/01/18.
 *
 */

public class PoiFilteringAdapter extends RecyclerView.Adapter<PoiFilteringAdapter.ViewHolder> {

    private ArrayList<Poi> mPoiList = new ArrayList<>();
    private PoiAdapterOnClickHandler mClickHandler;

    interface PoiAdapterOnClickHandler {
        void onClick(String eventClick);
    }

    public PoiFilteringAdapter(PoiAdapterOnClickHandler handler) {
        mClickHandler = handler;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView mPoiData;

        public ViewHolder(View view){
            super(view);
            mPoiData = view.findViewById(R.id.tv_poi_data);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            String eventInfo = "Poi name: " + mPoiList.get(adapterPosition).getName() +
                    "; event message: " + mPoiList.get(adapterPosition).getInfoHtml();
            mClickHandler.onClick(eventInfo);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layourIdForListItem = R.layout.poi_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);


        View view = inflater.inflate(layourIdForListItem, viewGroup, false);
        return new PoiFilteringAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String infoForThisEvent = mPoiList.get(position).getName();
        holder.mPoiData.setText(infoForThisEvent);
    }

    @Override
    public int getItemCount() {
        if(mPoiList == null) return 0;
        return mPoiList.size();
    }

    public void setPoiData(ArrayList<Poi> pois ){
        if (!pois.isEmpty() || pois != null) {
            mPoiList.addAll(pois);
        }

        notifyDataSetChanged();

    }


}
