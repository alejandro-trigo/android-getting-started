package com.example.alejandro.myapplication.getbuildingevents;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alejandro.myapplication.R;

import java.util.ArrayList;

import es.situm.sdk.v1.SitumEvent;

/**
 *
 * Created by alejandro on 5/01/18.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventAdapterViewHolder> {

    private ArrayList<SitumEvent> mEventData = new ArrayList<>();
    private EventAdapterOnClickHandler mClickHandler;

    // EVENTADAPTERONCLICKHANDLER INTERFACE
    interface EventAdapterOnClickHandler {
        void onClick(String eventClick);
    }
    // END EVENTADAPTERONCLICKHANDLER INTERFACE


    public EventAdapter(EventAdapterOnClickHandler handler) { mClickHandler = handler; }


    // EVENTADAPTERVIEWHOLDER CLASS
    public class EventAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final TextView mEventTextView;

        private EventAdapterViewHolder(View view){
            super(view);
            mEventTextView = view.findViewById(R.id.tv_event_data);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            String eventInfo = "Event name: " + mEventData.get(adapterPosition).getName() +
                    "; event message: " + mEventData.get(adapterPosition).getHtml();
            mClickHandler.onClick(eventInfo);

        }
    }
    //END EVENTADAPTERVIEWHOLDER CLASS



    @Override
    public EventAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        Context context = viewGroup.getContext();
        int layourIdForListItem = R.layout.event_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);


        View view = inflater.inflate(layourIdForListItem, viewGroup, false);
        return new EventAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventAdapterViewHolder holder, int position) {
        String infoForThisEvent = mEventData.get(position).getName();
        holder.mEventTextView.setText(infoForThisEvent);
    }

    @Override
    public int getItemCount() {
        if(mEventData == null) return 0;
        return mEventData.size();
    }

    public void setEventData(ArrayList<SitumEvent> events ){
        if (!events.isEmpty() || events != null) {
            mEventData.addAll(events);
        }

        notifyDataSetChanged();

    }



}
