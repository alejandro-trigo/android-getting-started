package com.example.alejandro.myapplication;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alejandro.myapplication.drawbuilding.DrawBuildingActivity;
import com.example.alejandro.myapplication.drawposition.DrawPositionActivity;
import com.example.alejandro.myapplication.getbuildingevents.GetBuildingEvents;
import com.example.alejandro.myapplication.poifiltering.PoiFilteringActivity;
import com.example.alejandro.myapplication.positioning.PositioningActivity;
import com.example.alejandro.myapplication.indooroutdoor.IndoorOutdoorActivity;
import com.example.alejandro.myapplication.realtime.RealTimeActivity;

import java.util.ArrayList;
import java.util.List;

import es.situm.sdk.SitumSdk;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SitumSdk.init(this);
        setup();
    }

    private void setup(){
        List<Sample> items = new ArrayList<>();
        items.add(new Sample("Indoor positioning", PositioningActivity.class));
        items.add(new Sample("Indoor-Outdoor positioning", IndoorOutdoorActivity.class));
        items.add(new Sample("Draw Position", DrawPositionActivity.class));
        items.add(new Sample("Draw Building", DrawBuildingActivity.class));
        items.add(new Sample("Draw realtime devices over the map", RealTimeActivity.class));
        items.add(new Sample("Show all the events in a building", GetBuildingEvents.class));
        items.add(new Sample("Show Filtered Pois of a building", PoiFilteringActivity.class));

        recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager((new LinearLayoutManager(MainActivity.this)));
        recyclerView.setAdapter(new SamplesAdapter(items, this));
    }

    @Override
    public void onClick(View view) {
        Class clazz = (Class) view.getTag();
        startActivity(new Intent(MainActivity.this, clazz));
    }

    private static class SamplesAdapter extends RecyclerView.Adapter<SampleViewHolder>{
        private List<Sample> items;
        private View.OnClickListener onClickListener;

        SamplesAdapter(List<Sample> items,View.OnClickListener onClickListener){
            this.items = items;
            this.onClickListener = onClickListener;
        }


        @Override
        public SampleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_sample, parent, false);
            return new SampleViewHolder(view, onClickListener);
        }

        @Override
        public void onBindViewHolder(SampleViewHolder holder, int position) {
            holder.fill(items.get(position));
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    private static class SampleViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;

        SampleViewHolder(View itemView, View.OnClickListener onClickListener){
            super(itemView);
            itemView.setOnClickListener(onClickListener);
            textView = (TextView) itemView.findViewById(R.id.cell_sample_text);
        }

        void fill(Sample sample){
            itemView.setTag(sample.getClazz());
            textView.setText(sample.getText());
        }

    }
}
