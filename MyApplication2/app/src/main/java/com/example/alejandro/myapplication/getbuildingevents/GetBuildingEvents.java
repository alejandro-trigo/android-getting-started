package com.example.alejandro.myapplication.getbuildingevents;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alejandro.myapplication.R;

import java.util.ArrayList;
import java.util.Collection;

import es.situm.sdk.SitumSdk;
import es.situm.sdk.error.Error;
import es.situm.sdk.model.cartography.Building;
import es.situm.sdk.utils.Handler;
import es.situm.sdk.v1.SitumEvent;



/**
 *
 * Created by alejandro on 4/01/18.
 */

public class GetBuildingEvents extends AppCompatActivity implements EventAdapter.EventAdapterOnClickHandler{

    private static final String TAG = GetBuildingEvents.class.getSimpleName();
    private static final String BUILDING_ID = "2742";

    private RecyclerView mRecyclerView;
    private TextView mErrorMessage;
    private ProgressBar mProgressBar;

    private Building building;
    ArrayList<SitumEvent> events = new ArrayList<>();

    private EventAdapter eventAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_events);

        mRecyclerView = findViewById(R.id.rv_event_list) ;
        mErrorMessage = findViewById(R.id.tv_error_message);
        mProgressBar = findViewById(R.id.pb_loading_indicator);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        eventAdapter = new EventAdapter(this);

        mRecyclerView.setAdapter(eventAdapter);

        loadEventData();

    }

    private void loadEventData(){
        showEventDataView();
        getEvents();
        if(!events.isEmpty()){
            showEventDataView();
            eventAdapter.setEventData(events);
        }
    }



    @Override
    public void onClick(String eventData){
        Context context = this;
        Toast.makeText(context, eventData, Toast.LENGTH_SHORT).show();
    }

    private void showEventDataView(){
        mErrorMessage.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage(){
        mRecyclerView.setVisibility(View.INVISIBLE);
        mErrorMessage.setVisibility(View.VISIBLE);
    }

    private void getEvents() {

        SitumSdk.communicationManager().fetchBuildings(new Handler<Collection<Building>>() {

            @Override
            public void onSuccess(Collection<Building> buildings) {
                Log.d(TAG, "onSuccess: buildings fetched");
                for (Building bui : buildings) {
                    if (bui.getIdentifier().equals(BUILDING_ID)) {
                        building = bui;
                    }
                }

                SitumSdk.communicationManager().fetchEventsFromBuilding(building, new Handler<Collection<SitumEvent>>(){

                    @Override
                    public void onSuccess(Collection<SitumEvent> situmEvents) {
                        for(SitumEvent event : situmEvents){
                            events.add(event);
                            Log.i(TAG, "onSuccess: event: " + event);
                        }
                        eventAdapter.setEventData(events);
                        mProgressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onFailure(Error error) {
                        Log.e(TAG, "onFailure: fetching events: " + error);
                        showErrorMessage();
                    }
                });

            }

            @Override
            public void onFailure(Error error) {
                Log.e(TAG, "onFailure: fetching buildings: " + error);
                showErrorMessage();
            }
        });

    }



    // MENU OPTIONS

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_refresh){
            eventAdapter.setEventData(new ArrayList<SitumEvent>());
            loadEventData();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // END MENU OPTIONS
}
