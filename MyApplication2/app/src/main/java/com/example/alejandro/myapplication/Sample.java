package com.example.alejandro.myapplication;

/**
 * Created by alejandro on 22/12/17.
 */

public class Sample {
    private String text;
    private Class clazz;

    public Sample(String text, Class clazz){
        this.text = text;
        this.clazz = clazz;
    }

    public String getText(){
        return text;
    }

    public Class getClazz(){
        return clazz;
    }
}
