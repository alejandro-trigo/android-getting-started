package com.example.alejandro.myapplication.poifiltering;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alejandro.myapplication.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import es.situm.sdk.SitumSdk;
import es.situm.sdk.error.Error;
import es.situm.sdk.model.cartography.Building;
import es.situm.sdk.model.cartography.Poi;
import es.situm.sdk.utils.Handler;


/**
 * Created by alejandro on 8/01/18.
 *
 */

public class PoiFilteringActivity extends AppCompatActivity implements PoiFilteringAdapter.PoiAdapterOnClickHandler{

    private static final String TAG = PoiFilteringActivity.class.getSimpleName();
    private static final String BUILDING_ID = "2742";

    private RecyclerView mRecyclerViewPOI;
    private TextView mErrorMessage;
    private ProgressBar mProgressBar;

    private Building building;
    ArrayList<Poi> poiList = new ArrayList<>();

    private PoiFilteringAdapter poiFilteringAdapter;

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_pois);

        mRecyclerViewPOI = findViewById(R.id.rv_poi_list);
        mErrorMessage = findViewById(R.id.tv_error_message2);
        mProgressBar = findViewById(R.id.pb_loading_pois) ;

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mRecyclerViewPOI.setLayoutManager(layoutManager);
        mRecyclerViewPOI.setHasFixedSize(true);

        poiFilteringAdapter = new PoiFilteringAdapter(this);

        mRecyclerViewPOI.setAdapter(poiFilteringAdapter);

        loadPoisData();

    }

    public void loadPoisData(){
        showPoiDataView();
        getPoiList();
        if(!poiList.isEmpty()){
            showPoiDataView();
            poiFilteringAdapter.setPoiData(poiList);
        }
    }

    @Override
    public void onClick(String poiData) {
        Context context = this;
        Toast.makeText(context, poiData, Toast.LENGTH_SHORT).show();
    }


    private void showPoiDataView(){
        mErrorMessage.setVisibility(View.INVISIBLE);
        mRecyclerViewPOI.setVisibility(View.VISIBLE);
    }

    private void showPoiErrorMessage(){
        mRecyclerViewPOI.setVisibility(View.INVISIBLE);
        mErrorMessage.setVisibility(View.VISIBLE);
    }

    private void getPoiList() {


        SitumSdk.communicationManager().fetchBuildings(new Handler<Collection<Building>>() {

            @Override
            public void onSuccess(Collection<Building> buildings) {
                Log.d(TAG, "onSuccess: buildings fetched");
                for (Building bui : buildings) {
                    if (bui.getIdentifier().equals(BUILDING_ID)) {
                        building = bui;
                    }
                }

                SitumSdk.communicationManager().fetchIndoorPOIsFromBuilding(building, new Handler<Collection<Poi>>(){

                    @Override
                    public void onSuccess(Collection<Poi> pois) {
                        for(Poi poi : pois){
                            poiList.add(poi);
                            Log.i(TAG, "onSuccess: poi: " + poi);
                        }
                        poiList = filterPois(poiList);
                        poiFilteringAdapter.setPoiData(poiList);
                        mProgressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onFailure(Error error) {
                        Log.e(TAG, "onFailure: fetching pois: " + error);
                        showPoiErrorMessage();
                    }
                });

            }

            @Override
            public void onFailure(Error error) {
                Log.e(TAG, "onFailure: fetching buildings: " + error);
            }
        });
    }

    public ArrayList<Poi> filterPois(ArrayList<Poi> mPoiList){
        Map<String, String> poiKV;
        ArrayList<Poi> poiList = new ArrayList<>();
        for(Poi p : mPoiList){
            poiKV = p.getCustomFields();
            if(!poiKV.isEmpty()) {
                if (poiKV.get("Name").equals("Situm")) {
                    poiList.add(p);
                    Log.i(TAG, " KEY - VALUE " + poiKV.get("Name"));
                }
            }
        }
        return poiList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_refresh){
            poiFilteringAdapter.setPoiData(new ArrayList<Poi>());
            loadPoisData();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
